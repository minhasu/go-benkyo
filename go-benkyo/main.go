package main

import (
	"fmt"

	"./animal"
)

func main() {
	fmt.Println(AppName())
	fmt.Println(animal.ElephantFeed())
	fmt.Println(animal.RabbitFeed())
	fmt.Println(animal.MonkeyFeed())
}
