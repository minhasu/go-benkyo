package main

import (
	"fmt"
)

type Point struct{ X, Y int }

// Pointという構造体だがポインターではない
// func (p Point) Set(x, y int) {
func (p *Point) Set(x, y int) {
	p.X = x
	p.Y = y
}
func main() {
	// 型が違うのに、Goではいい感じにやってくれる
	// ただし値渡しになるため参照しない
	p1 := Point{}
	p1.Set(1, 2)
	fmt.Println(p1)

	// slice型とポイントは親和性が高い
	ps := make([]Point, 5)
	for _, p := range ps {
		fmt.Println(p.X, p.Y)
	}
}
