package main

func main() {
	// var x interface{}
	// x = "ss"
	// fmt.Println(x)
	// x = 1
	// fmt.Println(x)
	// x = '山'
	// fmt.Println(x)
	// x = true
	// fmt.Println(x)

	// var y, z interface{}
	// y, z = 1, 3
	// z := y + z
	// fmt.Println(y)
	// fmt.Println(z)
	a := plus(1, 5)
	println(a)
	hello()
	// q, err := div(19, 7)
	// q, _ := div(19, 7)
	// _, _ = div(19, 7)
	// fmt.Println("%v", q)
	// _, err = div(19, 7)
	// if err!={
	// 	// エラー処理
	// }

	println(doSomething(3))
}

func plus(x, y int) int {
	return x + y
}

func hello() {
	println("Hello")
}

func div(a, b int) (int, int) {
	q := a / b
	r := a % b
	return q, r
}

func doSomething(a int) int {
	return a
}

func do3(_, _ int) int {
	return 1
}
