package main

import (
	"fmt"
	"runtime"
)

func main() {
	/**
	go sub() //ゴルーチン: 非同期処理
	i := 0
	for i < 100 {
		fmt.Println("main loop")
		i++
	}
	**/

	go fmt.Println("aaa")
	fmt.Printf("NumCPU %d\n", runtime.NumCPU())
	// 非同期処理が走っている数
	fmt.Printf("NumGoRoutine %d\n", runtime.NumGoroutine())
	fmt.Printf("Version %d\n", runtime.Version())

	// go func() {
	// 	k := 0
	// 	for k < 100 {
	// 		println("sub2 loop")
	// 	}
	// }()
}

func sub() {
	i := 0
	for i < 100 {
		fmt.Println("sub Loop")
		i++
	}
}

// Redis：キーとバリューしかないDB
// 非同期処理をちゃんと理解すればゴルーチンを使える
// キューとスタックがたくさん溜まっていて、順番は関係ないもの
// 非同期をたくさん書くとコードのみやすさが減る
// Goの言語特性と非同期処理をちゃんとわかっていればいい感じの非同期処理が書ける
