package main

import (
	"fmt"
)

func main() {
	// defer: 遅延実行、関数が終わったときに実行するもの
	defer fmt.Println("on defer")
	// deferも実行されない
	// os.Exit
	panic("runtime error!")
	fmt.Println("hello")

	// recover: panicから回復させる
}
