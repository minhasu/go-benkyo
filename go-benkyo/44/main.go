package main

import (
	"fmt"
)

type T struct {
	Id   int
	Name string
}

func (t *T) String() string {
	return fmt.Sprintf("<<%d, %s>>", t.Id, t.Name)
}

func main() {
	t := &T{10, "Taro"}
	fmt.Println(t)
}
