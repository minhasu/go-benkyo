package main

import (
	"fmt"
)

// ToStringというメソッドを持ったインターフェース
type Stringify interface {
	// interfaceを定義する場合、メソッドを使用しなければならない
	// Stringifyインターフェースを使用する場合、ToStringメソッドを使用しなければならない
	ToString() string
}

type Person struct {
	Name string
	Age  int
}

type Car struct {
	Number string
	Model  string
}

// インターフェースが持っている関数なのでメソッド
func (p *Person) ToString() string {
	return fmt.Sprintf("%s(%d)", p.Name, p.Age)
}

func (c *Car) ToString() string {
	return fmt.Sprintf("[%s]%s", c.Number, c.Model)
}

func main() {
	vs := []Stringify{
		&Person{Name: "Taro", Age: 21},
		&Car{Number: "XXX-123", Model: "PX512"},
	}

	for _, v := range vs {
		fmt.Println(v.ToString())
	}
}
