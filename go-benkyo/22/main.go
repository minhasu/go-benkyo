package main

func main() {
	// 配列のループがsliceでも使える
	s := []string{"apple", "banana", "cherry"}
	for _, v := range s {
		s = append(s, "Melon")
		println(v)
	}

	// 無限ループ
	// for i := 0; i < len(s); i++ {
	// 	println(s[i])
	// 	s = append(s, "Melon")
	// }
}
