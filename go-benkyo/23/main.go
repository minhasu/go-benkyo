package main

import (
	"fmt"
)

func main() {
	println(sum(1, 2, 3))
	println(sum(1, 2, 3, 4, 5))
	println(sum())
	s := []int{1, 2, 3}
	println(sum(s...))

	a := [...]int{1, 2, 3}
	pow(a)
	fmt.Println(a)

	// 参照渡し
	a1 := []int{1, 2, 3}
	pow2(a1)
	fmt.Println(a1)

	var (
		b  [3]int
		b1 []int
	)
	fmt.Println(b)
	// nil；ポインタで、なにも定義していないからnilになる
	fmt.Println(b1)
	fmt.Println(b1 == nil)

	// これは配列
	c := [5]int{1, 2, 3, 4, 5}
	// これは場所を参照しているからslice型
	s = c[0:2]
	fmt.Println(c)
	fmt.Println(s)
	c[1] = 10
	fmt.Println(s)

	// 拡張した場合
	// 参照する側が、参照元より拡張された場合、値渡しとなる
	s = append(s, 15)
	c[1] = 20
	fmt.Println(s)
}

// 引数を可変slice型で取れる関数
// 返り値の型を指定することは出来ないが、引数の型を指定することはできる
// 引数に可変な配列をもたせることも可能
// slice型は最初からポインタになっている
func sum(s ...int) int {
	n := 0
	for _, v := range s {
		n += v
	}
	return n
}

func pow(a [3]int) {
	for i, v := range a {
		a[i] = v * v
	}
	return
}

func pow2(a []int) {
	for i, v := range a {
		a[i] = v * v
	}
	return
}
