package main

import (
	"fmt"
)

func main() {
	// var b bool
	// b = true
	// b := false

	/**
	int8
	int16
	int32
	int64

	uint8
	uint16
	uint32
	uint64

	byte

	float32
	float64


	**/

	// n := float64(255)
	// println(n)

	zero := 0.0
	pinf := 1.0 / zero
	ninf := -1.0 / zero
	nan := zero / zero
	fmt.Println("%v %v %v\n", pinf, ninf, nan)

	// 指数
	print(1E6)

	// ルーン型
	c := '松'
	fmt.Println(c)

}
