package main

/**
type error interface{
	Erro() string
}
*/
type MyError struct {
	Message string
	ErrCode int
}

func (e *MyError) Error() string {
	return e.Message
}

func RaiseError() error {
	return &MyError{Message: "エラー発生", ErrCode: 1234}
}

func tmp() (err error) {
	// どんな構造体を作ったとしても, errorというメソッドを持っていればOK
	return
}

// インターフェース、phpとかとほぼ一緒
// Goのインターフェースはメソッドしか持たない
func main() {

}
