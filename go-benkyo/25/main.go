package main

import (
	"fmt"
)

// channel型
// 非同期処理を簡単にできる型
// 999がないのは送信するまでにreceiver関数が死んでしまうから
func main() {
	// var (
	// 	ch  chan int   // どちらでもいける。下の2つに代入できる
	// 	ch1 <-chan int // 送信型
	// 	ch2 chan<- int // 受信型
	// )

	ch := make(chan int)
	go receiver(ch)
	// 非同期処理を分散することで処理速度を上げることができる
	go receiver(ch)
	go receiver(ch)
	go receiver(ch)
	i := 0
	for i < 1000 {
		ch <- i
		i++
	}
}

func receiver(ch <-chan int) {
	for {
		i := <-ch
		fmt.Println(i)
	}
}
