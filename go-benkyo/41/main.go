package main

import (
	"fmt"
)

type User struct {
	Id   int
	Name string
}

func main() {
	// User型であることが明示的なため、引数の型指定をしなくてよい
	m1 := map[User]string{
		{Id: 1, Name: "Taro"}: "Tokyo",
		{Id: 2, Name: "Jiro"}: "Osaka",
	}
	m2 := map[int]User{
		1: User{Id: 11, Name: "aa"},
	}
	m3 := map[int]map[int]string{
		1: {1: "Apple", 2: "Banana"},
		2: {1: "Grape", 2: "Cherry"},
	}
	fmt.Println(m1)
	fmt.Println(m2)
	fmt.Println(m3)
}
