package main

import "fmt"

type Point struct {
	X, Y int
}

type FloatPoint struct{ X, Y float64 }

func (p *Point) Rendor() {
	fmt.Printf("<%d, %d>, \n", p.X, p.Y)
}

func (p *FloatPoint) Rendor() {
	fmt.Printf("<%f, %f>, \n", p.X, p.Y)
}

func main() {
	// 同じメソッドwを持つ配列を作れる
	p := &Point{X: 5, Y: 13}
	p.Rendor()
	p1 := &FloatPoint{X: 5, Y: 13}
	p1.Rendor()

}
