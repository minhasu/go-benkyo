package main

import (
	"fmt"
)

type Person struct {
	Id   int
	Name string
	Area string
}

func main() {
	// pointerになる
	p := new(Person)
	fmt.Println(p.Id)
	fmt.Println(p.Name)
	fmt.Println(p.Area)
	// &を使っても初期化する
	// newも&も一緒
	i := new(int)
	fmt.Println(i)
}
