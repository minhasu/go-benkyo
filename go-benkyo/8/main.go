package main

import "fmt"

func main() {
	// s := "hello \n world"
	// fmt.Println(s)
	// s1 := `
	// Goの
	// 文字列リテルによる
	// 複数行の文字列
	// `
	// fmt.Println(s1)

	// a := [5]int{1, 2, 3, 4, 5}
	// fmt.Println(a)
	// var a1 string
	// fmt.Println(a1)
	// var a2 [3]int
	// fmt.Println(a2)

	// d := [...]int{1, 2, 3, 4, 5}
	// d := [5]int{1, 2, 3, 4, 5}
	// fmt.Println(d)

	// d1 := [...]uint8{1, 2, 3}
	// d1[0] = 255

	// var (
	// 	d2 [5]int
	// 	d3 [5]int
	// 	// d3 [5]uint
	// )
	// d2 = d3
	// fmt.Println(d2)

	// e := [3]int{1, 2, 3}
	// e1 := [3]int{4, 5, 6}
	// e = e1
	// fmt.Println(e)

	// e[0] = 0
	// e[1] = 0
	// fmt.Println(e)
	// fmt.Println(e1)

	// interface{}
	var x interface{}
	fmt.Println(x)
}
