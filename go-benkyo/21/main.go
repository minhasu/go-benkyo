package main

import (
	"fmt"
)

func main() {

	// 参照型
	// slice, map, channel
	// make()

	// slice：可変
	// sliceとarrayは別物
	// var a []int
	s := make([]int, 5, 10)
	fmt.Println(s)
	// len: 長さを図るメソッド、phpのcount
	fmt.Println(len(s))

	a := [3]int{1, 2, 3}
	fmt.Println(len(a))

	s1 := []int{1, 2, 3}
	fmt.Println(s1)

	a1 := [5]int{1, 2, 3, 4, 5}
	s2 := a1[0:3]
	fmt.Println(s2)

	fmt.Println(a1[len(a1)-2])

	s3 := "ABCDE"[1:3]
	fmt.Println(s3)

	s4 := "あいうえお"
	fmt.Println(s4)

	s5 := []int{1, 2, 3}
	s5 = append(s5, 4)
	fmt.Println(s5)

	s5 = append(s5, 5, 6, 7)
	fmt.Println(s5)

	s6 := []int{1, 2, 3}
	s7 := []int{2, 3, 4}
	// slice　配列をSLI
	s8 := append(s6, s7...)
	fmt.Println(s8)
	// 参照型を値渡し
	// 参照型は、場所を指定していなくても参照できる

}
