package main

const a = 1

func main() {
	const (
		X = 1
		Y = 1
		Z = 1
	)
	// 省略型（すべて１）
	const (
		x = 1
		y
		z
	)

	const (
		x1 = 1
		y1
		z1
		xx = "あ"
		yy
	)

	// ルーン型
	const (
		x3 = `aaa
		fafa
		
		fafa`
	)

	// iota
	// enum（数値と名前を対応付ける）
	// コンパイルエラーのときなどに
	const (
		NameErr = 1
		TypeErr
		LengthErr
		D = 17
		E = iota
		F
	)
	println(NameErr)
	println(TypeErr)
	println(LengthErr)
	println(D)
	println(E)
	println(F)

}
