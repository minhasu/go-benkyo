package main

func main() {
	p := &Point{X: 1, Y: 2}
	swap(p)
	p.Y = y
}

type Point struct {
	X, Y int
}

func swap(p *Point) {
	x, y := p.Y, p.X
	p.X = x
	p.Y = y
}
