package main

import (
	"fmt"
)

var plusAlias = plus

func plus(x, y int) int {
	return x + y
}

func main() {
	// 型推論
	f := func(x, y int) int { return x + y }
	fmt.Printf("%T", f)

	// var x func(int, int) int
	// fmt.Printf("%T", x)

	// 即時関数
	// 冗長に書ける
	println(func(x, y int) int { return x + y }(2, 5))

}
