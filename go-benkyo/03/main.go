package main

import "fmt"

func main() {
	// コメント
	a := [3]string{"aaaa", "bbbb", "cccc"}
	v := [3]string{
		"aaa",
		"bbb",
		"ccc",
	}
	fmt.Println(a)
	fmt.Println(v)
}
