package main

import (
	"fmt"
)

/**
type I0 interface {
	Method() int
}

// interfaceを継承する場合はメソッドは実装する
// I1を継承するときは、MethodとMethos2を実装する必要がある
type I1 interface {
	I0
	Method2() int
}

type I2 interface {
	I1
	Method3() int
}
**/

type T struct{ ID int }

func (t *T) GetId() int {
	return t.ID
}

func main() {
	t := &T{ID: 19}
	showId(t)
}

func showId(id *T) {
	fmt.Println(id.GetId())
}

// interfaceはポインタ（参照型）
