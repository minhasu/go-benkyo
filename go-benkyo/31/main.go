package main

import (
	"fmt"
)

func main() {
	// 型にエイリアスは貼れるが、エイリアス間の互換性はない
	// type T0 int
	// type T1 int

	// t0 := T0(5)
	// i0 := int(t0)

	// t1 := T1(8)
	// i1 := int(t1)

	// t0 = t1

	// 構造体もクラスのようにクラス（構造体）型を持てる
	type Point struct {
		X, Y int
	}

	var pt Point
	pt.X = 10
	fmt.Println(pt)

	// 初期値を生成できる
	// X, Y ; フィールド
	pt2 := Point{X: 1, Y: 2}
	fmt.Println(pt2)

	type Person struct {
		ID   uint
		name string
		部署   string
	}
	p := Person{ID: 17, 部署: "営業", name: "aa"}
	fmt.Println(p)

	type Feed struct {
		Name   string
		Amount uint
	}
	// 構造体が構造体を持つパターン
	type Animal struct {
		Name string
		Feed Feed
		// Feed // 自明なので省略できる
	}
	a := Animal{
		Name: "Monkey",
		Feed: Feed{
			Name:   "Banana",
			Amount: 10,
		},
	}
	fmt.Println(a)
	println(a.Name)
	println(a.Feed.Name)
	println(a.Feed.Amount)
	// println(a.Amount)

	type T0 struct {
		Name1 string
	}
	type T1 struct {
		T0
		Name2 string
	}
	type T2 struct {
		T1
		Name3 string
	}
	t := T2{T1: T1{T0: T0{Name1: "X"}, Name2: "Y"}, Name3: "Z"}
	fmt.Println(t.Name1)
	fmt.Println(t.Name2)
	fmt.Println(t.Name3)

	// 暗黙の型指定
	// 自明な型を定義していると、型の指定がなくなる
	// 上記の構造体の定義もそういうこと
	type T struct {
		int
		string
		float64
	}
	s := T{111, "文字列", 3.14}
	println(s.int)
	println(s.string)
	println(s.float64)

}
