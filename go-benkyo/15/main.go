package main

import (
	"fmt"
	// "math"
	// パッケージ名に変数をつけられる
	// aa "math"
	// 関数名がかぶらない場合はパッケージ名を省略できる
	// . "math"
)

func main() {
	fmt.Println("hello")
	println(AorB())
	// fmt.Println(math.Pi)
	// fmt.Println(aa.Pi)
	// fmt.Println(Pi)

}

func AorB() (b string) {
	b = "A"
	{
		b = "B"
		return b
	}
	return
}
