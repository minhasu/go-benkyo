package main

import (
	"fmt"
)

func main() {
	type Base struct {
		Id    int
		Owner string
	}

	type A struct {
		Base
		Name string
		Area string
	}

	type B struct {
		Base
		Title  string
		Bodies []string
	}

	a := A{
		Base: Base{17, "Taro"},
		Name: "Taro",
		Area: "Tokyo",
	}

	b := B{
		Base:   Base{71, "Jiro"},
		Title:  "no title",
		Bodies: []string{"A", "B"},
	}

	// IdとOwnerの型指定がなく一意なため省略して書ける
	println(a.Id)
	println(a.Owner)
	println(b.Id)
	println(b.Owner)

	/*
		struct {
			T1
			*T2
			P.T3 // Pはパッケージ
			*P.T4 // Pパッケージのポインタを指定
		}
	*/

	// 無限ループ
	// type T3 struct {
	// 	T1
	// }
	// type T1 struct {
	// 	T3
	// }

	// typeはエイリアス
	s := struct{ X, Y int }{X: 1, Y: 23}
	showStruct(s)

	type Point struct {
		X, Y int
	}
	p := Point{X: 3, Y: 10}
	showStruct(p)
	// エイリアスに互換性は必要
	// 元は同じでも、エイリアスが一緒である必要がある

}

func showStruct(s struct{ X, Y int }) {
	fmt.Println(s)
}
