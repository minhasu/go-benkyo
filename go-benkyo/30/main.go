package main

type callBack func(i int) int

// callback: 関数を引数として関数に渡して実行する関数
// callbackは自由に返り値を決められる
func sum(ints []int, c callBack) int {
	var sum int
	for _, i := range ints {
		sum += i
	}
	return c(sum)
}

func main() {
	// 構造体 struct
	// ほぼクラスと一緒。クラスの継承がないやつ
	// 構造体では値渡し
	// オブジェクト指向ではない、ただの独立したパーツみたいな。
	// goではオブジェクト指向がないからそれみたいに書くことができる

	// int型にaliasを設定している
	type MyInt int
	// rgb: red, green, blue <-uint型
	// 普段使わないから、uint型としてrbg型を定義しておけば使いやすい

	var n1 MyInt = 5
	n2 := MyInt(7)
	println(n1)
	println(n2)

	type (
		// 大文字と小文字の違いは、外部パッケージから呼べるかどうか
		IntPair     [2]int
		Strings     []string // slice
		areaMap     map[string][2]float64
		IntsChannel chan []int
	)
	_ = IntPair{1, 2}
	_ = Strings{"Apple", "Banana"}
	_ = areaMap{"Tokyo": {35.1111, 12.111}}
	_ = make(IntsChannel)

}
