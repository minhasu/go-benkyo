package main

import "fmt"

func main() {
	/**
	i := 0
	for {
		println(i)
		if i == 100 {
			break
		}
	}
	for i < 100 {
		println(i)
	}
	// 無限ループ
	k := 0
	for k <= 100 {
		k++
	}
	*/
	fruits := [3]string{"apple", "banana", "grape"}
	for i, s := range fruits {
		println(i)
		println(s)
	}
	for i, r := range "ABC" {
		fmt.Printf("%d %d", i, r)
	}
	// n := 3
	// switch n {
	// case 1, 2:
	// 	println("1 or 2")
	// case 3, 4:
	// 	println("3 or 4")
	// default:
	// 	println("unknown")
	// }
	switch n := 3; n {
	case 1, 2:
		println("1 or 2")
	case 3, 4:
		// case n < 0 && n > 3:
		println("3 or 4")
		// 処理を中断し、下の処理に行く
		fallthrough
	default:
		println("unknown")
	}

	// 型の判定でもswitch文を用いる
	var x interface{} = 3
	// i, isInt := x.(int)
	// f := x.(float32)
	// println(i)
	// println(isInt)
	switch v := x.(type) {
	case bool:
		print("bool")
		print(v)
	// case int, uint:
	case int:
		print("int, uint")
		print(v * v)
		// print(v)
	case string:
		print("string")
		print(v)
	default:
		print("default")
	}

	// 飛べるのは同一の関数内のだけ
	// ラベルはよく使う
	println("A")
	goto L
	println("B")
L:
	println("C")
	// goto文は基本的に非推奨だが、for文のみでやってもいい
	for {
		for {
			for {
				goto DONE
			}
		}
	}
DONE:
}
