package main

import (
	"fmt"
)

var n = 100 // パッケージ変数
var N = 1   // パッケージ外でも使える変数

func main() {
	// var a int
	// b := 1

	// var a = 1
	// a := 1 // 型推論
	// println(a)
	n = n + 1
	fmt.Println(n)

}
