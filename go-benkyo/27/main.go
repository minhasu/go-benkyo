package main

import (
	"fmt"
)

func main() {
	// ** : pointerの箱の中身
	// *** : pointerの箱の中身のpointer
	var p *int
	fmt.Println(p == nil)
	// CやC++との互換性を保つため
	var (
		_ *[]string
		_ *map[int]rune // literal、single構文
		_ *chan int
	)
	var i int
	p1 := &i
	fmt.Printf("%T\n", p1)
	// pinterのpointer
	p2 := &p1
	fmt.Printf("%T\n", p2)

	var i3 int
	p3 := &i3
	fmt.Println(*p3)
	*p3 = 10
	fmt.Println(i3)

	i4 := 1
	inc(&i4)
	inc(&i4)
	inc(&i4)
	inc(&i4)
	inc(&i4)
	inc(&i4)
	inc(&i4)
	fmt.Println(i4)
}

// int型のpointerを参照する関数
func inc(p *int) {
	*p++
}
