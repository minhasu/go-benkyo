package animal

import (
	// testingと書くと、ユニットテスト用になる
	// ok / fail
	// go test -v ./animal/
	// カバレッジ計測
	// go test --cover ./animal/
	"testing"
)

func TestElephantFeed(t *testing.T) {
	expect := "Grass"
	// actual := ElephantFeed()
	actual := ElephantFeed()
	if expect != actual {
		t.Errorf("%s != %s", expect, actual)
	}
}
