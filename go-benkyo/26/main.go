package main

import (
	"fmt"
)

func main() {
	// channelは受信型
	// 非同期
	// make: 参照型を作成する
	// chanel: 取り出しが自由な参照型の配列みたいなもの
	// マルチスレッド：
	// メッセージ：
	// サイズを決めることができる、closeすることも、サイズを指定しなくてもＯＫ（1個のキャパになる）
	// channelは送受信で1：1
	// channelのメリットは、並行処理を簡単にかける
	// closeしたchannelから受信すると、channelの型のデフォルト値が入る
	// m. ok := <-ch ： close前後でboolの値が違う(前：true, 後：false)）
	// receiveでもchannelの受信ができる<-非同期で早くなる仕組み
	ch := make(chan int, 3)
	ch <- 4
	ch <- 5
	ch <- 6
	i := <-ch
	j := <-ch
	k := <-ch
	fmt.Println(i)
	fmt.Println(j)
	fmt.Println(k)

	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)

	// ch1 <- 1
	// ch2 <- 2

	// ch2から受信した整数を2倍してch2へ送信
	go func() {
		for {
			i := <-ch1
			ch2 <- (i * 2)
		}
	}()

	// ch2から受信した整数を1減算、ch3へ送信
	go func() {
		for {
			i := <-ch2
			ch3 <- (i - 1)
		}
	}()

	// channelの受信遅れに対応するselect文がある
	n := 1
LOOP:
	for {
		select {
		// 整数を増分させながらch1へ送信
		case ch1 <- n:
			n++
			// ch3から受信した整数を出力
		case i := <-ch3:
			fmt.Println("received", i)
		default:
			if n > 100 {
				break LOOP
			}
		}

	}
}
