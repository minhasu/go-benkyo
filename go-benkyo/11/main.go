package main

import (
	"fmt"
)

func main() {
	// f := returnFunc()
	// f()
	// これでも動く
	// returnFunc()()

	// コールバック関数
	callFunc(func(x int) {
		fmt.Println("call function")
	})
}

func returnFunc() func() {
	return func() {
		fmt.Println("I am developer")
	}
}

func callFunc(f func(x int)) {
	// 非同期処理を書く
	f(1)
}
