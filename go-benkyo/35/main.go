package main

import (
	"fmt"
)

type Point struct {
	X, Y int
}

// 構造体やクラスが持つ関数をメソッド
func (p *Point) Rendor() {
	fmt.Printf("<%d, %d>, \n", p.X, p.Y)
}
func main() {
	p := &Point{X: 5, Y: 13}
	p.Rendor()

}
