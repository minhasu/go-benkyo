package main

import (
	"fmt"
)

type MyInt int

// Recieverを張ればエイリアスとメソッドが使える
func (m MyInt) Plus(i int) int {
	return int(m) + 1
}

type Point struct{ X, Y int }

func (p *Point) ToString() string {
	return fmt.Sprintf("[%d], [%d]", p.X, p.Y)
}

func main() {
	// 明示的にMyIntのエイリアス型をとった形を表している
	// println(MyInt(4).Plus(3))
	p := Point{10, 19}
	println(p.ToString())

	// レシーバー
	f := (*Point).ToString
	// 返り値はStringでないといけないから怒られる
	fmt.Printf("%T", f)

	println(f(&Point{X: 7, Y: 11}))
	println((*Point).ToString(&Point{11, 12}))
}
