package main

func main() {
	p := &[3]int{1, 2, 3}
	println((*p)[1])
	// goの場合はアドレス番号をあまり指定しない ※配列の場合
	println(p[1])
	// 配列のcapacityを測るやつ
	// 配列の場合はcap=lenだが、sliceの場合はcap=lenとは限らない
	println(cap(p))
	println(len(p))

	for _, v := range p {
		println(v)
	}

}
