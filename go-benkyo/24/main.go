package main

import (
	"fmt"
)

// map型(key: value)
func main() {
	m := make(map[int]string)
	m[1] = "US"
	m[81] = "Japan"
	m[86] = "China"
	fmt.Println(m)

	m1 := map[int]string{
		1: "Hello",
		5: "KKK", // 配列の最後はカンマをつけるか波括弧で終わる
	}
	fmt.Println(m1)

	m2 := map[int][]int{
		1: []int{1},
		3: []int{1, 2},
		4: []int{1, 2, 3},
	}
	fmt.Println(m2)

	// mapのvalueにmapもあり
	m3 := map[int]map[float64]string{
		1: {3.14: "円周率"},
	}
	fmt.Println(m3)

	m4 := map[int]string{1: "A", 2: "B"}
	fmt.Println(m4[1])
	fmt.Println(m4[10]) // 空文字が出る

	m5 := map[int]int{1: 0}
	fmt.Println(m5[4]) // 0が出る
	s := m5[10]
	fmt.Println(s)

	for k, v := range m {
		fmt.Println(k)
		fmt.Println(v)
	}
	fmt.Println(len(m))
	delete(m, 81) // deleteは参照的に破壊する
	fmt.Println(m)

}
